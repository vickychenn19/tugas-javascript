
/**
 * 
 * Diberikan function ubahHuruf(kata) yang akan menerima satu parameter berupa string.
 * Function akan me-return sebuah kata baru dimana setiap huruf akan digantikan dengan huruf alfabet setelahnya.
 * Contoh, huruf a akan menjadi b, c akan menjadi d, k menjadi l, dan z menjadi a.
 * 
 */


function ubahHuruf(kata) {
    var arr = [];
    var x = kata.split("");
    for(var j = 0 ;j<x.length ; j++){  
        switch(x[j]) {
            case 'z':
                x[j] = 'a';
                arr.push(x[j]);
            break;
            case ' ':
            
            break;
            default:
              x[j] = String.fromCharCode(1 + x[j].charCodeAt(0));
              arr.push(x[j]);
          }
    }
    return(arr);
}
  
// TEST CASES
console.log(ubahHuruf('wow')); // xpx
console.log(ubahHuruf('developer')); // efwfmpqfs
console.log(ubahHuruf('javascript')); // kbwbtdsjqu
console.log(ubahHuruf('keren')); // lfsfo
console.log(ubahHuruf('semangat')); // tfnbohbu
